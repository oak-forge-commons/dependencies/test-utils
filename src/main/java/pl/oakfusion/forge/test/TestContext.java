package pl.oakfusion.forge.test;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.bus.MessageHandler;
import pl.oakfusion.data.message.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class TestContext {
    private final MessageBus messageBus;
    private Message message;
    private final List<Class<? extends Message>> receivedMessagesClasses = new ArrayList<>();

    public TestContext(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public <M extends Message> void postingMessage(M message) {
        this.message = message;
    }

    public <M extends Message, H extends MessageHandler<M>> void handlerForMessage (Class<M> messageClass, H handler) {
        messageBus.registerMessageHandler(messageClass, handler);
    }

    public <M extends Message> boolean received(Class<M> messageClass, Consumer<M> handler) {
        messageBus.registerMessageHandler(messageClass, m -> {
            handler.accept(m);
            if(!receivedMessagesClasses.contains(messageClass)){
                receivedMessagesClasses.add(messageClass);
            }
        });
        performTest();
        return receivedMessagesClasses.contains(messageClass);
    }

    void performTest() {
        if (this.message != null) {
            this.messageBus.post(this.message);
        }

    }
}

package pl.oakfusion.forge.test;

import com.google.common.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.bus.guava.GuavaMessageBus;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

public class TestContextTest {
    MessageBus messageBus = new GuavaMessageBus(new EventBus());
    TestContext testContext = new TestContext(messageBus);

    @Test
    public void should_return_false_when_calling_received_without_calling_posting_message_before(){
        assertFalse(testContext.received(TestMessages.MockEvent.class, m -> {}));
    }

    @Test
    public void should_return_false_and_run_handler_of_posted_message_when_calling_received_with_posting_message_not_posting_to_the_bus(){
        AtomicInteger counter = new AtomicInteger(0);
        testContext.handlerForMessage(TestMessages.MockCommand.class, message -> { counter.getAndIncrement();});
        testContext.postingMessage(new TestMessages.MockCommand());
        assertFalse(testContext.received(TestMessages.MockEvent.class, m -> {}));
        assertEquals(1, counter.get());
    }

    @Test
    public void should_return_true_when_calling_received_with_posting_message_posting_to_the_bus(){
        testContext.handlerForMessage(TestMessages.MockCommand.class, message -> {
            messageBus.post(new TestMessages.MockEvent());});
        testContext.postingMessage(new TestMessages.MockCommand());
        assertTrue(testContext.received(TestMessages.MockEvent.class, m -> {}));
    }

    @Test
    public void should_return_false_when_calling_received_with_posting_message_posting_to_the_bus(){
        testContext.handlerForMessage(TestMessages.MockCommand.class, message -> {
            messageBus.post(new TestMessages.MockEvent());
        });
        testContext.postingMessage(new TestMessages.MockCommand());
        assertTrue(testContext.received(TestMessages.MockEvent.class, m -> {}));
        assertFalse(testContext.received(TestMessages.MockEvent2.class, m -> {}));
    }

    @Test
    public void should_return_true_when_calling_received_with_posting_message_posting_two_events_to_the_bus(){
        testContext.handlerForMessage(TestMessages.MockCommand.class, message -> {
            messageBus.post(new TestMessages.MockEvent());
            messageBus.post(new TestMessages.MockEvent2());
        });
        testContext.postingMessage(new TestMessages.MockCommand());
        assertTrue(testContext.received(TestMessages.MockEvent.class, m -> {}));
        assertTrue(testContext.received(TestMessages.MockEvent2.class, m -> {}));
    }
}

package pl.oakfusion.forge.test;

import com.google.common.eventbus.EventBus;
import org.junit.jupiter.api.Test;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.bus.guava.GuavaMessageBus;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class TestFixtureTest {
    MessageBus messageBus = new GuavaMessageBus(new EventBus());
    TestFixture testFixture = new TestFixture(messageBus);

    @Test
    public void should_throw_null_pointer_exception_when_running_then_without_passed_given(){
        assertThrows(NullPointerException.class, () -> testFixture.then(() -> {}));
    }

    @Test
    public void should_run_runnable_passed_to_given_and_to_then(){
        AtomicInteger counter = new AtomicInteger(0);
        testFixture.given(counter::getAndIncrement).then(counter::getAndIncrement);
        assertEquals(2, counter.get());
    }

    @Test
    public void should_run_runnable_passed_to_given_when_and_then(){
        AtomicInteger counter = new AtomicInteger(0);
        testFixture.given(counter::getAndIncrement).when(counter::getAndIncrement).then(counter::getAndIncrement);
        assertEquals(3, counter.get());
    }

    @Test
    public void should_throw_assertion_exception_when_handler_not_called(){
        AtomicInteger counter = new AtomicInteger(0);
        assertThrows(AssertionError.class, () -> testFixture.given(counter::getAndIncrement).whenPostingCommand(new TestMessages.MockCommand()).
                thenOnEvent(TestMessages.MockEvent.class, event -> {}));
        assertEquals(1, counter.get());
    }

    @Test
    public void should_run_handler_for_event(){
        AtomicInteger counter = new AtomicInteger(0);
        messageBus.registerMessageHandler(TestMessages.MockCommand.class, message -> messageBus.post(new TestMessages.MockEvent()));
        testFixture.given(counter::getAndIncrement).whenPostingCommand(new TestMessages.MockCommand()).
                thenOnEvent(TestMessages.MockEvent.class, event -> {counter.getAndIncrement();});
        assertEquals(2, counter.get());
    }

}

